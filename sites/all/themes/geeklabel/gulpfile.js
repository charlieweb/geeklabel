/*global -$ */
'use strict';
var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var browserSync = require('browser-sync');
var reload = browserSync.reload;
var eventStream = require('event-stream');






// styles task, will run when any SCSS files change & BrowserSync
// will auto-update browsers
gulp.task('styles', function () {
    return gulp.src('scss/**/*.scss')
        .pipe($.sourcemaps.init())
        .pipe($.sass({
            outputStyle: 'nested', // libsass doesn't support expanded yet
            precision: 10,
            includePaths: ['.'],
            onError: function (err) { notify().write(err); console.error.bind(console, 'Sass error:'+err);}
        }))
        .pipe($.postcss([
            require('autoprefixer-core')({browsers: ['last 2 version']})
         ]))
        .pipe($.sourcemaps.write())
        .pipe(gulp.dest('css'))
        .pipe($.filter('scss**/*.css'))
        .pipe(browserSync.reload({stream:true}));
});

gulp.task('jshint', function () {
    return gulp.src('js/**/*.js')
        .pipe(reload({stream: true, once: true}))
        .pipe($.jshint())
        .pipe($.jshint.reporter('jshint-stylish'))
        .pipe($.if(!browserSync.active, $.jshint.reporter('fail')));
});


gulp.task('images', function () {
    return gulp.src('images/**/*')
        .pipe($.newer('build/'))
        .pipe($.cache($.imagemin({
            progressive: true,
            interlaced: true,
            svgoPlugins: [{removeViewBox: false}]
        })))
        .pipe(gulp.dest('images'));
});

gulp.task('fonts', function () {
    return gulp.src('fonts/**/*')
        .pipe($.filter('**/*.{eot,svg,ttf,woff}'))
        .pipe(gulp.dest('fonts'));
});



gulp.task('bs-reload', function (){
    browserSync.reload();
});

gulp.task('browser-sync', function(){
    //watch files
    var files = [
        'css/**/*.css',
        'js/**/*js',
        'images/**/*',
        'templates/*.tpl.php'
    ];

    return browserSync.init(files, {
        proxy: "http://localhost:8888", //change this to whatever your local development URL is.
        open: false,
        injectChanges: true
    });
});



gulp.task('watch',  ['images', 'fonts', 'styles'],function () {

    gulp.watch('js/ts/**/*.ts', ['scripts']);
    gulp.watch('scss/**/*.scss', ['styles']);
    gulp.watch('images/**/*', ['images']);
    gulp.watch('scripts/vendor/**/*', ['extras']);
    gulp.watch('fonts/*', ['fonts']);
});

gulp.task('clear', function (done) {
    return $.cache.clearAll(done);
});

gulp.task('cleanFonts', require('del').bind(null, ['fonts']));


gulp.task('clean', require('del').bind(null, ['.tmp', 'build']));

gulp.task('build', ['images', 'fonts', 'styles'], function () {
    return gulp.src('/**/*').pipe($.size({title: 'build', gzip: true}));
});

gulp.task('default', ['clear', 'clean'], function () {
    gulp.start('build');
});
